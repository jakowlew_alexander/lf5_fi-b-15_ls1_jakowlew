import java. util. Scanner; 
public class AufgabenZuSchleifen2 {

	public static void main(String[] args) {
		
	
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Aufgaben zu Schleifen 2\n");
		System.out.println("Welche Aufgabe soll durchlaufen?");
		System.out.println("Aufgabe 1: Zaehlen = 1");
		System.out.println("Aufgabe 2: Fakultaet = 2\nAufgabe 3: Quersumme = 3");
		System.out.println("Aufgabe 4: Temperaturumrechnung = 4");
		int z = scan.nextInt();
		switch(z) {
		case 1:
		System.out.println("\n\nAufgabe 1: Zaehlen");
		System.out.print("Geben Sie eine Zahl zu der hoch bzw. runtergezaehlt werden soll: ");
		int g = scan.nextInt();
		int h = 0;
		System.out.print("a) ");
		while (g>=h) {
			System.out.print(h+", ");
			h++;
		}
		System.out.println("");
		System.out.print("b) ");
		h-=1;
		while (h>=0) {
			System.out.print(h+", ");
			h--;
		}
		break;
		
		case 2:
		
		System.out.println("\n\nAufgabe 2: Fakultaet");
		System.out.print("Bitte Zahl bis maximal 20 angeben. ");
		
		long a = scan.nextLong();
		long b = 1;
		int c = 1;
		
		if (a>20) {
			System.out.println("Die Zahl ist zu gro�.");
		}
		else { 
			while (c<=a) {
				if (a>20) {
					System.out.println("Die Zahl ist zu gro�.");
					break;
				}
				b = b*c;
				System.out.println(b);
				c++;
				}
				System.out.println("Die Fakult�t ist "+b);	
		}
		break;
		
		case 3:
		System.out.println("\n\nAufgabe 3: Quersumme");
		System.out.println("Geben sie eine Zahl ein");
		int d = 0;
		int e = scan.nextInt();
		int f = e;
		
		do {
			d=d+(e%10);
			e = e/10;
			
		}
		while (0!=e);
		System.out.println("Die Quersumme von "+f+" ist "+ d);
		break;
		
		case 4:
		System.out.println("\n\nAufgabe 4: Temperaturumrechnung");
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		double start = scan.nextDouble();
		System.out.print("Bitte den Endwert in Celsius eingeben: ");
		double end = scan.nextDouble();
		System.out.print("Bitte die Schrittweite in Grad Celsius eingeben: ");
		double schritt = scan.nextDouble();
		double fahrenheit = 0.0;
		
		while (start<=end) {
			fahrenheit = start*1.8+32;
			System.out.printf("%.2f�C			%.2f�F\n", start, fahrenheit);
			start+=schritt;	
		}
		break;
	}
	}
}
