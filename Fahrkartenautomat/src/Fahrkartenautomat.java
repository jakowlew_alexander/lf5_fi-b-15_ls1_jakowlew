﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag =0; 
       double eingezahlterGesamtbetrag=0;
       double eingeworfeneMünze=0;
       double rückgabebetrag=0;
       byte anzahlTickets=0; //byte reicht vollkommen aus, da jemand wohl kaum über 128 Tickets kauft

       /*
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();	//zurzeit nur der Preis eines Tickets
       System.out.print("Anzahl von Tickets: ");
       anzahlTickets = tastatur.nextByte();
       zuZahlenderBetrag *= anzahlTickets; 	//zuZahlenderBetrag ist zurzeit noch der Preis des einzelnen Tickets
       */									//gerechnet wird zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets
       										//damit haben wir den Gesamtpreis
       zuZahlenderBetrag=fahrkartenbestellungErfassen();
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
      /* eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }*/

       // Fahrscheinausgabe
       // -----------------
     /*  System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
*/
       fahrkartenAusgeben();
       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeldAusgeben(rückgabebetrag, eingezahlterGesamtbetrag, zuZahlenderBetrag);
     /*  rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n" , rückgabebetrag );
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
                   */
       
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	System.out.print("Zu zahlender Betrag (EURO): ");
        double x = tastatur.nextDouble();	//zurzeit nur der Preis eines Tickets
        if (x<=0) {
        	x = 1;
        	System.out.println("Fehler! Ticketpreis wird auf 1 EURO gesetzt");
        }
        System.out.print("Anzahl von Tickets: ");
        byte y = tastatur.nextByte();
        if (y<10 && y>1) {
        	x *= y;
        }
   
        else {
        	y = 1;
        	System.out.println("Nur Werte von 1-10 möglich\nEs wird 1 Ticket berchnet\n");
        }
        return x;
    }
    
    public static double fahrkartenBezahlen(double y) {
    	Scanner tastatur = new Scanner(System.in);
        double x = 0.0;
        while(x < y)
        {
     	   System.out.printf("Noch zu zahlen: %.2f\n" , (y - x));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double z = tastatur.nextDouble();
     	  while (z<0.05 || z>2.00) {
     		  System.out.printf("Der Betrag ist hoch oder zu niedrig ");
     		  z = tastatur.nextDouble();
     	  }
           x += z;
           
        }
        
        return x;
    }
    
    public static void warte(int millisekunden) {
    	millisekunden = millisekunden/8;
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(millisekunden);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
       /* for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        */
    	warte(2000);
        System.out.println("\n\n");
    }
    public static void rueckgeldAusgeben(double x, double y, double z) {
        x = y - z;
        if(x > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n" , x );
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(x >= 2.0) // 2 EURO-Münzen
            {
         	  muenzeAusgeben(2, "EURO");
 	          x -= 2.0;
            }
            while(x >= 1.0) // 1 EURO-Münzen
            {
         	  muenzeAusgeben(1, "EURO");
 	          x -= 1.0;
            }
            while(x >= 0.5) // 50 CENT-Münzen
            {
         	  muenzeAusgeben(50, "CENT");
 	          x -= 0.5;
            }
            while(x >= 0.2) // 20 CENT-Münzen
            {
         	  muenzeAusgeben(20, "CENT");
  	          x -= 0.2;
            }
            while(x >= 0.1) // 10 CENT-Münzen
            {
         	  muenzeAusgeben(10, "CENT");
 	          x -= 0.1;
            }
            
            while(x >= 0.05)// 5 CENT-Münzen
            {
         	 // System.out.println("5 CENT");
         	  muenzeAusgeben(5, "CENT");
         	 // System.out.println("test");
  	          x -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static void muenzeAusgeben (int Betrag, String Einheit) {
    	
    	System.out.println(Betrag+" "+Einheit);
    }
}

//5.byte reicht vollkommen aus, da jemand wohl kaum über 128 Tickets kauft

//6.zuZahlenderBetrag ist zurzeit noch der Preis des einzelnen Tickets
//gerechnet wird zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets
//damit haben wir den Gesamtpreis