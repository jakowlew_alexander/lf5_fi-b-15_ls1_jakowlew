
public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = "\nAuch dies ist ein Satz.";
		
		System.out.printf("Das ist ein \"Satz\".\n");
		System.out.printf("Das ist auch ein Satz."+s);
	//println() erzeugt eine neue Zeile w�hrend print() keine neue Zeile anf�ngt
		
		String a = "*";
		String b = "***";
		String c = "*****";
		String d = "*******";
		String e = "*********";
		String f = "***********";
		String g = "*************";
		
		System.out.printf("\n\n%15s", a);
		System.out.printf("\n%16s"+"", b);
		System.out.printf("\n%17s"+"", c);
		System.out.printf("\n%18s"+"", d);
		System.out.printf("\n%19s"+"", e);
		System.out.printf("\n%20s"+"", f);
		System.out.printf("\n%21s"+"", g);
		System.out.printf("\n%16s"+"", b);
		System.out.printf("\n%16s"+"", b);
		
		double h = 22.4234234;
		double i = 111.2222;
		double j = 4.0;
		double k = 1000000.551;
		double l = 97.34;
		
		System.out.printf( "\n\n%.2f" , h);
		System.out.printf( "\n%.2f" , i);
		System.out.printf( "\n%.2f" , j);
		System.out.printf( "\n%.2f" , k);
		System.out.printf( "\n%.2f" , l);
	}
}
