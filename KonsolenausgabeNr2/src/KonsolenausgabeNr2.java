
public class KonsolenausgabeNr2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = "**";
		System.out.printf( "%4s\n", s );
		System.out.println("*    *");
		System.out.println("*    *");
		System.out.printf( "%4s\n\n", s );
		
		
		
		System.out.printf("%-5s= %-19s=%4d\n", "0!", "" ,1);
		System.out.printf("%-5s= %-19d=%4d\n", "1!", 1 ,1);
		System.out.printf("%-5s= %-19s=%4d\n", "2!", "1 * 2" ,2);
		System.out.printf("%-5s= %-19s=%4d\n", "3!", "1 * 2 * 3" ,6);
		System.out.printf("%-5s= %-19s=%4d\n", "4!", "1 * 2 * 3 * 4" ,24);
		System.out.printf("%-5s= %-19s=%4d\n\n", "4!", "1 * 2 * 3 * 4 * 5" ,120);
		
	/*	
	 	String a = "0!";
		String b = "=                   =";
		String c = "1";
		
		String d = "1!";
		String e = "= 1                 =";
	 
	  	System.out.printf( "%-5s", a);
		System.out.printf( "%-19s", b);
		System.out.printf( "%4s\n", c);
		
		System.out.printf( "%-5s", d);
		System.out.printf( "%-19s", e);
		System.out.printf( "%4s", c);
		
		kein guter Weg, stattdessen siehe oben.
	*/	
		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsius");
		System.out.println("------------------------");
		System.out.printf("%-12s|%10.2f\n", "-20", -28.8889);
		System.out.printf("%-12s|%10.2f\n", "-10", -23.3333);
		System.out.printf("%-12s|%10.2f\n", "+0", -17.7778);
		System.out.printf("%-12s|%10.2f\n", "+20", -6.6667);
		System.out.printf("%-12s|%10.2f\n", "+30", -1.1111);
	}

}
