import java.util.Scanner;

public class PCHaendler {
	static Scanner myScanner = new Scanner(System.in);
	public static void main(String[] args) {
		//Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String Bestellanfrage = "Was m�chten Sie bestellen?";
		String artikel = ""; // = myScanner.next();
		artikel = liesString(Bestellanfrage);

		String Bestellanzahl = "Geben Sie die Anzahl ein:";
		int anzahl = 0;
		anzahl = liesInt(Bestellanzahl);

		String Nettopreis = "Geben Sie den Nettopreis ein:";
		double preis = 0.0;
		preis = liesDouble(Nettopreis);

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		/*System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		*/rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}
	
	public static String liesString(String text) {
		String a;
		System.out.println(text);
		a = myScanner.next();
		return a;
	}
	
	public static int liesInt(String text) {
		System.out.println(text);
		int a = myScanner.nextInt();
		return a;
	}
	
	public static double liesDouble(String text) {
		System.out.println(text);
		double a = myScanner.nextDouble();
		return a;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double a = anzahl * nettopreis;
		return a;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double a = nettogesamtpreis * (1 + mwst /100);
		return a;
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}
