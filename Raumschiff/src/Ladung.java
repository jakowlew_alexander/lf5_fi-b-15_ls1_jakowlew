
public class Ladung {

	private String bezeichnung;
	private int menge;
	
	public Ladung() {
		this.bezeichnung = "";
		this.menge = 0;
	}
	
	public Ladung(String bez, int menge) {
		this.bezeichnung = bez;
		this.menge = menge;
	}
	
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public int getMenge() {
		return this.menge;
	}
	
	public void setMenge(int menge) {
		this.menge = menge;
	}
}
