import java.util.*;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcast =  new ArrayList<String>();
	private ArrayList<Ladung> ladungsVerzeichnis = new ArrayList<Ladung>(); 
	
	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenssystemeInProzent = 0;
		this.androidenAnzahl = 0;
		this.schiffsname = "";
		this.broadcast = new ArrayList<String>();
		this.ladungsVerzeichnis = new ArrayList<Ladung>();
		
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieInProzent, int schildeInProzent, int huelleInProzent, int lebenssystemeInProzent, int androidenAnzahl, String schiffsname, ArrayList<Ladung> ladungsVerzeichnis) {
	
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieInProzent = energieInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenssystemeInProzent = lebenssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.broadcast = new ArrayList<String>();
		this.ladungsVerzeichnis = ladungsVerzeichnis;
		
		

}
	public int getPhotonentorpedoAnzahl () {
		return this.photonentorpedoAnzahl;
	}
	
	public void setPhotonentorpedoAnzahl (int pho) {
		this.photonentorpedoAnzahl = pho;
	}

	public int getEnergieInProzent() {
		return this.energieInProzent;
	}
	
	public void setEnergieInProzent(int ener) {
		this.energieInProzent = ener;
	}
	
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	
	public void setSchildeInProzent(int schild) {
		this.schildeInProzent=schild;
	}
	
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	
	public void setHuelleInProzent(int huelle) {
		this.huelleInProzent = huelle;
	}
	
	public int getLebenssystemeInProzent () {
		return this.lebenssystemeInProzent;
	}
	
	public void setLebenssystemeInProzent(int leben) {
		this.lebenssystemeInProzent = leben;
	}
	
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	
	public void setAndroidenAnzahl(int anzahl) {
		this.androidenAnzahl = anzahl;
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	public void setSchiffsname(String name) {
		this.schiffsname = name;
	}
	
	public ArrayList<String> getBroadcast() {
		return this.broadcast;
	}
	
	public void setBroadcast(ArrayList<String> broad) {
		this.broadcast = broad;
	}
	
	public ArrayList<Ladung> getLadungsVerzeichnis() {
		return this.ladungsVerzeichnis;
	}
	
	public void setLadungsVerzeichnis(ArrayList<Ladung> ladung) {
		this.ladungsVerzeichnis = ladung;
	}
	
	public void zustandAusgeben() {
		System.out.println("-------------------------------------------------------------------------------------------------------");
		System.out.println("Zustand des Schiffes "+this.schiffsname+" wird ausgegeben\n");
		System.out.println("Anzahl der Photonentorpedos: "+this.getPhotonentorpedoAnzahl());
		System.out.println("Energie in Prozent: "+this.getEnergieInProzent());
		System.out.println("Schilde in Prozent: "+this.getSchildeInProzent());
		System.out.println("Huellenstatus in Prozent: "+this.getHuelleInProzent());
		System.out.println("Status der Lebenssysteme in Prozent: "+this.getLebenssystemeInProzent());
		System.out.println("Anzahl der Andoiden: "+this.getAndroidenAnzahl());
		System.out.println("Name des Schiffs: "+this.getSchiffsname());
		System.out.println("Broadcast: "+this.getBroadcast());
		
		System.out.print("Ladungsverzeichnis: ");
		for (int i=0;i<this.ladungsVerzeichnis.size();i++) {
		System.out.print(this.getLadungsVerzeichnis().get(i).getBezeichnung()+", ");
		}
		System.out.println("");
		System.out.println("-------------------------------------------------------------------------------------------------------\n");
	}
	
	 
	
	public void ladeverzeichnisAusgeben() {
		System.out.print("-------------------------------------------------------------------------------------------------------");
		System.out.println("\nLadungsverzeichnis von "+this.schiffsname+" wird ausgegeben");
		System.out.print("Ladungsverzeichnis: ");
		for (int i=0;i<this.ladungsVerzeichnis.size();i++) {
		System.out.print(this.getLadungsVerzeichnis().get(i).getBezeichnung()+", ");
		}
		System.out.println("");
		System.out.print("-------------------------------------------------------------------------------------------------------\n\n");
	}
	
	public void trefferVermerken () {
		if (this.schildeInProzent<=0) {
			this.huelleInProzent=this.huelleInProzent-50;
			this.energieInProzent=this.energieInProzent-50;
		}
			else {
				this.schildeInProzent=this.schildeInProzent-50;
			}
		if (this.huelleInProzent<=0) {
		this.setLebenssystemeInProzent(0);
		System.out.println(this.schiffsname+" wurde getroffen!");
		nachrichtAnAlle("Lebenerhaltungssysteme wurden zerst�rt");
		}
	}
	
	public void nachrichtAnAlle(String nachricht) {
		this.broadcast.add(nachricht);
		
	}
	
	public void photonentorpedosAbschiessen(Raumschiff ziel) {
		if (this.photonentorpedoAnzahl == 0) 
			nachrichtAnAlle("=*Click*=-");
			else {
				this.photonentorpedoAnzahl--;
				nachrichtAnAlle("Photonentorpedo abgeschossen");
				ziel.trefferVermerken();
			}
			
	}
	
	public void phaserkanoneAbschiessen (Raumschiff ziel) {
		if (this.energieInProzent<50)
		nachrichtAnAlle("=*Click*=-");
		else this.energieInProzent=this.energieInProzent-50;
		ziel.trefferVermerken();
	}
	
	public void broadcastAuslesen () {
		System.out.println("-------------------------------------------------------------------------------------------------------");
		System.out.println("Broadcast wird ausgelesen\n");
		for (int i=0;i<this.broadcast.size();i++) {
			System.out.println(this.broadcast.get(i));
		}
		System.out.println("-------------------------------------------------------------------------------------------------------");
	}
	
	public void ladungsverzeichnisAufrauemen() {
		System.out.println("-------------------------------------------------------------------------------------------------------");
		System.out.println("Ladungsverzeichnis von "+this.schiffsname+" wird ger�umt");
		for (int i=0; i<this.ladungsVerzeichnis.size();i++) {
		if (this.ladungsVerzeichnis.get(i).getMenge()==0); {
			System.out.println(this.ladungsVerzeichnis.get(i).getBezeichnung()+" wurde aus Ladungszeichnis entfernt");
			this.ladungsVerzeichnis.remove(i);	
			}
		}
		System.out.println("-------------------------------------------------------------------------------------------------------\n");
	}
		
	/*public void photonentorpedosEinsetzen(int anzahl) {
		
		for (int i =0;i<this.ladungsVerzeichnis.size();i++) {
		if (this.ladungsVerzeichnis.get(i).getBezeichnung()=="Photonentorpedo") {
			if (this.ladungsVerzeichnis.get(i).getMenge()<=0)
			System.out.println("Keine Photonentorpedos gefunden");
			nachrichtAnAlle("-=*Click*=-");
		}
		else 
		}
		if (this.ladungsVerzeichnis.contains(La))) {
		System.out.println("Keine Photonentorpedos gefunden!");
		nachrichtAnAlle("-=*Click*=-");
		}
		else {
		if (anzahl>this.photonentorpedoAnzahl) {
			this.setPhotonentorpedoAnzahl(anzahl);
			}
		
		}
	}*/
	
	
}
	

