import java.util.ArrayList;
import java.util.Arrays;

public class Test {

	public static void main(String[] args){
        
		//Objekte instanzieren und deklarieren
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		Ladung l3 = new Ladung("Rote Materie",2);
		Ladung l4 = new Ladung("Forschungssonde",35);
		Ladung l5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung l6 = new Ladung("Plasma-Waffe",50);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		
		ArrayList<Ladung> la1 = new ArrayList<Ladung>();
		la1.add(l1);
		la1.add(l5);
		
		ArrayList<Ladung> la2 = new ArrayList<Ladung>();
		la2.add(l2);
		la2.add(l3);
		la2.add(l6);
		
		ArrayList<Ladung> la3 = new ArrayList<Ladung>();
		la3.add(l4);
		
		
		Raumschiff klingonen= new Raumschiff(1,100,100,100,100,2,"IKS Hegh'ta",la1);
		Raumschiff romulaner= new Raumschiff(2,100,100,100,100,2,"IRW Khazara",la2);
		Raumschiff vulkanier= new Raumschiff(0,80,80,50,100,5,"Ni'Var",la3);
		
		//Klingonen schie�en einmal auf die Romulaner
		klingonen.photonentorpedosAbschiessen(romulaner);
		romulaner.phaserkanoneAbschiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		
		klingonen.zustandAusgeben();
		klingonen.ladeverzeichnisAusgeben();
		vulkanier.ladungsverzeichnisAufrauemen();
		klingonen.photonentorpedosAbschiessen(romulaner);
		klingonen.photonentorpedosAbschiessen(romulaner);
		klingonen.zustandAusgeben();
		klingonen.ladeverzeichnisAusgeben();
		romulaner.zustandAusgeben();
		romulaner.ladeverzeichnisAusgeben();
		vulkanier.zustandAusgeben();
		vulkanier.ladeverzeichnisAusgeben();
		
		
		
		//Broadcast wurde vorher als Methode f�r Klasse Raumschiff angegeben?
		klingonen.broadcastAuslesen();
		
		
    }

}
