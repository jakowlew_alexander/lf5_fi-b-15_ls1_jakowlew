import java.util.Scanner; 

public class ScannerUebung {

	public static void main(String[] args) {
		 // Neues Scanner-Objekt myScanner wird erstellt
		 Scanner myScanner = new Scanner(System.in);

		 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

		 // Die Variable zahl1 speichert die erste Eingabe
		 int zahl1 = myScanner.nextInt();

		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

		 // Die Variable zahl2 speichert die zweite Eingabe
		 int zahl2 = myScanner.nextInt();

		 // Die Addition der Variablen zahl1 und zahl2
		 // wird der Variable ergebnis zugewiesen.
		 int ergebnis = zahl1 + zahl2;
		 int ergebnis2 = zahl1 * zahl2;
		 double zahl1double = zahl1;
		 double zahl2double = zahl2;
		 double ergebnis3 = zahl1double / zahl2double;
		 int ergebnis4 = zahl1 - zahl2;

		 System.out.print("\n\n\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
		 System.out.println("\nErgenbnis der Multiplikation lautet: " + zahl1+" * "+zahl2+" = "+ergebnis2);
		 System.out.println("Ergenbnis der Dividierung lautet: " + zahl1+" / "+zahl2+" = "+ergebnis3);
		 System.out.println("Ergenbnis der Substraktion lautet: " + zahl1+" - "+zahl2+" = "+ergebnis4);
		 myScanner.close();
		

	}

}
