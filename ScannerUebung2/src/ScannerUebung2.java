import java.util.Scanner;

public class ScannerUebung2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie ihren Namen ein.");
		String name = scan.next();
		
		System.out.println("Bitte geben Sie ihr Alter ein.");
		int alter = scan.nextInt();
		
		System.out.println("\nSie hei�en "+name+" und sind "+alter+" Jahre alt.");
	}

}
